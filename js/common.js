/*********************************************************************************
 Utility
**********************************************************************************/
// ブラウザーに関する情報
var brwsInfo = {
    f_win: false,
    f_ie: false,
    f_ff: false,
    f_chrome: false,
    f_ipad: false,
    f_iphone:false,
    f_ios: false,
    f_android: false,
    f_mobile: false,
    f_tablet: false,
    brws_ver: false,
    os_ver: false,
    //f_cookie: checkCookie(),
    get: null,
    construct: function() {
        var usrAg = window.navigator.userAgent.toLowerCase();
        var appVersion = window.navigator.appVersion.toLowerCase();
        var bodyObj = document.getElementsByTagName('body')[0];
        
        if (usrAg.match(/win(dows\s)?/i) !== null) {
            bodyObj.className = bodyObj.className + ' win';
            this.f_win = true;
        }
        if (usrAg.indexOf("msie") > -1) {
            this.f_ie = true;
            bodyObj.className = bodyObj.className + ' ie';
            try {
                this.brws_ver = usrAg.match(/msie\s(\d+)\./i)[1];
                bodyObj.className = bodyObj.className + ' ie_'+this.brws_ver;
            } catch(e) {}
        } else if (usrAg.indexOf('FireFox') > -1 || usrAg.indexOf('firefox') > -1) {
            this.f_ff = true;
            bodyObj.className = bodyObj.className + ' firefox';
        } else if (usrAg.indexOf('CriOS') > -1 || usrAg.indexOf('crios') > -1 || usrAg.indexOf('Chrome') > -1 || usrAg.indexOf('chrome') > -1) {
            this.f_chrome = true;
            bodyObj.className = bodyObj.className + ' chrome';
        } else if (usrAg.indexOf('Safari') > -1 || usrAg.indexOf('safari') > -1) {
            this.f_ff = true;
            bodyObj.className = bodyObj.className + ' safari';
        }
        if (usrAg.indexOf('iPad') > -1 || usrAg.indexOf('ipad') > -1) {
            this.f_ipad = true;
            this.f_tablet = true;
            this.f_mobile = true;
            this.f_ios = true;
            bodyObj.className = bodyObj.className + ' tablet mobile ipad';
        } else if (usrAg.indexOf('iPhone') > -1 || usrAg.indexOf('iphone') > -1) {
            this.f_iphone = true;
            this.f_mobile = true;
            this.f_ios = true;
            bodyObj.className = bodyObj.className + ' smart mobile iphone';
        } else if (usrAg.indexOf('Android') > -1 || usrAg.indexOf('android') > -1) {
            this.f_tablet = true;
            this.f_mobile = true;
            this.f_android = true;
            bodyObj.className = bodyObj.className + ' android';
            try {
                this.os_ver = usrAg.match(/android\s(\d+\.\d+)/i)[1];
                bodyObj.className = bodyObj.className + ' android_'+parseInt(this.os_ver);
            } catch(e) {}
        }
        
        // GET パラメータを取得する
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        if(hashes != location.href) {
            for(var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            this.get = vars;
        }
    }
};
// IE 対策
if(brwsInfo.f_ie) {
    if (!('console' in window)) {
        window.console = {};
        window.console.log = function(str){
            return str;
        };
    }
}


function ieInitPropaties() {
    // IEでページを読み込んだ直後に初期設定などがうまく反映されない時にここで設定する
    // fixedMenu.topFixedDisplayViewing_resetOffset();
}

/*********************************************************************************
 common part include
**********************************************************************************/


$(function() {
  $("#Header").load("/header.html");
  $("#Footer").load("/footer.html");
  $("#Footer-xs").load("/footer.html");
});

$(function() {
  $("#Navbar").load("/menu.html", generateNavbar);
});

/*********************************************************************************
 build link
**********************************************************************************/
/* Definition of Navbar content */
function NavbarContent(type, anchor, href) {
  this.Type = type;
  this.Anchor = anchor;
  this.Href = href;
  //this.Src = src;
}
/* Dynamic generator for Navigation Bar Content */
function generateNavbar(eventObject) {
  var content = new Array();
  var ulElem = document.getElementById('NavbarContent');
  var activeNum = $('#Navbar').data("active");
  
  content[0] = new NavbarContent(null, 'home', 'index.html');
  content[1] = new NavbarContent(null, 'news', 'news.html');
  content[2] = new NavbarContent(null, 'wine', 'wine.html');
  content[3] = new NavbarContent(null, 'food', 'food.html');
  content[4] = new NavbarContent(null, 'profile', 'profile.html');
  content[5] = new NavbarContent(null, 'access', 'access.html');

  var i = 0;
  for(i in content) {
    if(i == activeNum) {
      content[i] = new NavbarContent("active", content[i].Anchor, '#'); 
    }
    
    var liElem = document.createElement('li');
    var aElem = document.createElement('a');
    var spanElem = document.createElement('span');
    //var imgElem = document.createElement('img');
    
    if(content[i].Type != null) {
      liElem.className = content[i].Type;
    }
    aElem.textContent = content[i].Anchor;
    aElem.href = content[i].Href;
    //imgElem.src = content[i].Src;
    
    //aElem.appendChild(imgElem);
    liElem.appendChild(aElem);
    ulElem.appendChild(liElem);
  }
}

 /*********************************************************************************
 loading
**********************************************************************************/

$('head').append(
    '<style type="text/css">#container { display: none; } #fade, #loader { display: block; }</style>'
);
 
jQuery.event.add(window,"load",function() { // 全ての読み込み完了後に呼ばれる関数
    var pageH = $("#container").height();
    var w = $(window).width() * 1.05; //IEに対応させる為1.05倍
    $("#fade").css("height", pageH).delay(900).fadeOut(800);
    $("#loader").delay(600).fadeOut(300);
    $("#container").css("display", "block");

    //$('#container').attr('width', w);
});

 /*********************************************************************************
 rollover and svg to png
**********************************************************************************/
jQuery.fn.rollover = function(suffix) {
    suffix = suffix || '_on';
    var check = new RegExp(suffix + '\\.\\w+$');
    return this.each(function() {
        var img = jQuery(this);
        var src = img.attr('src');
        if (check.test(src)) return;
        var _on = src.replace(/\.\w+$/, suffix + '$&');
        jQuery('<img>').attr('src', _on);
        img.hover(
            function() { img.attr('src', _on); },
            function() { img.attr('src', src); }
        );
    });
};
jQuery(document).ready(function($) {
  $('.wineList-btn a img').rollover();
});

if (!Modernizr.svg) {
    $("img[src$='.svg']").each(function(){
        $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
    });    
}
