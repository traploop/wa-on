<?php
header("Content-Type: text/html; charset=UTF-8");
$mode = $_GET["mode"];


echo "以下のファイルのアップロードしました。<br />";
echo "<ul class='fileup' style='list-style: none;'>";
if ($mode == "up") {
	// ファイル処理
	// 複数ファイルのアップロード対応
	foreach ($_FILES["file"]["error"] as $key => $value) {
	    // アップロード成功した際の処理
	    if ($value == UPLOAD_ERR_OK) {
	        // ファイル名
	        $file_name = $_FILES["file"]["name"][$key];
	        // ファイルタイプ（MIME）
	        $file_type = $_FILES["file"]["type"][$key];
	        // ファイルサイズ（byte）
	        $file_size = $_FILES["file"]["size"][$key];
	        // 一時的に保存された場所へのパス
	        $file_temp = $_FILES["file"]["tmp_name"][$key];
	        
	        // 保存するファイル名 ( 今回はオリジナルのファイル名の前に upload を付加 )
	        $file = $file_name;

	        if (($result = move_uploaded_file($file_temp, $file)) === true) {
	            echo "<li style='color: #616161;'>";
	            echo $file_name."&nbsp;/&nbsp;";
	            //echo $file_type."&nbsp;/&nbsp;";
	            echo $file_size." byte";
	            echo "</li>";
	        } else {
	        	echo "<li>";
	            echo "ファイルのアップロードに失敗しました。";
	            echo "</li>";
	        }
	       
	    }
	}
}
echo "</ul>";
echo "<a href='./pdf_upload/file_upload.html'>フォームに戻る</a>";
?>
